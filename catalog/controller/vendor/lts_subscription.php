<?php
class ControllerVendorLtsSubscription extends controller {

  public function index() {  
    $this->load->language('vendor/lts_subscription');

    $this->load->model('vendor/lts_subscription');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->getList();
  }
  
  public function add() {

     $this->load->language('vendor/lts_subscription');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('vendor/lts_subscription');

    if ($this->request->server['REQUEST_METHOD'] == 'POST') {

      $results = $this->model_vendor_lts_subscription->getSubscription($this->request->get['subscription_id']);

      $data['expire_date'] = Date('y:m:d', strtotime('+'. $results['validity'] .'days'));

      $this->model_vendor_lts_subscription->addVendorPlan($data['expire_date'], $results);

      $this->response->redirect($this->url->link('vendor/lts_subscription/invoice&subscription_id=' . $this->request->get['subscription_id']  , ''));
    }

    $this->getList();
  } 

  public function getList() {



    if (!$this->config->get('module_lts_vendor_status')) {
      $this->response->redirect($this->url->link('error/not_found', '', true));
    }
      
    if (!$this->customer->getId() || !$this->customer->isVendor()) {
      $this->response->redirect($this->url->link('vendor/lts_login', '', true));
    }

    $results = $this->model_vendor_lts_subscription->getSubscriptions();

    $data['current_plan'] = $this->model_vendor_lts_subscription->getVendonActivePlan($this->customer->getId());


    // print_r($data['current_plan']['subscription_id']);

    // die;

    $data['languages'] = $this->model_localisation_language->getLanguages();
    


    foreach ($results as $result) {
      $data['subscriptions'][] = array(
          'name'              => $result['name'],
          'subscription_id'   => $result['subscription_id'],
          'no_of_product'     => $result['no_of_product'],
          'join_fee'          => $this->currency->format( $result['join_fee'], $this->config->get('config_currency')),
          'subscription_fee'  => $this->currency->format( $result['subscription_fee'], $this->config->get('config_currency')),
          'validity'          => $result['validity'],
          'subscribe'          => $this->url->link('vendor/lts_subscription/subscribe', '&subscription_id=' . $result['subscription_id']),
      );
    }

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/home')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('vendor/lts_subscription', '', true)
    ); 


    $this->load->controller('vendor/lts_header/script');
    $data['footer'] = $this->load->controller('common/footer');
    $data['header'] = $this->load->controller('common/header');
    $data['lts_column_left'] = $this->load->controller('vendor/lts_column_left');


    $this->response->setOutput($this->load->view('vendor/lts_subscription', $data));
  }

  public function subscribe() {

    $this->load->language('vendor/lts_subscription');

    $this->load->model('vendor/lts_subscription');

    $this->document->setTitle($this->language->get('heading_title'));

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/home')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('vendor/lts_subscription', '', true)
    ); 

    $results = $this->model_vendor_lts_subscription->getSubscription($this->request->get['subscription_id']);

    if(!empty($results['no_of_product'])) {
        $data['product']  = $results['no_of_product'];
    } else {
        $data['product']  = 0;
    }

    if(!empty($results['join_fee'])) {
        $data['join_fee']  = $this->currency->format( $results['join_fee'], $this->config->get('config_currency'));
    } else {
        $data['join_fee']  = 0;
    }

    if(!empty($results['subscription_fee'])) {
        $data['subscription_fee']  = $this->currency->format( $results['subscription_fee'], $this->config->get('config_currency'));
    } else {
        $data['subscription_fee']  = 0;
    } 
    if(!empty($results['validity'])) {
        $data['validity']  = $results['validity'];
    } else {
        $data['validity']  = 0;
    }

    $data['action'] = $this->url->link('vendor/lts_subscription/add', '&subscription_id=' . $this->request->get['subscription_id']);

    $this->load->controller('vendor/lts_header/script');
    $data['footer'] = $this->load->controller('common/footer');
    $data['header'] = $this->load->controller('common/header');
    $data['lts_column_left'] = $this->load->controller('vendor/lts_column_left');
    
    $this->response->setOutput($this->load->view('vendor/lts_subscription_confirmation', $data));
  }

   public function invoice() {
    $this->load->language('vendor/lts_subscription_invoice');

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/home')
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('vendor/lts_subscription', '', true)
    ); 

    $data['config_name']  = $this->config->get('config_name');
    $data['config_address']  = $this->config->get('config_address');
    $data['config_telephone']  = $this->config->get('config_telephone');
    $data['config_email']  = $this->config->get('config_email');

    $this->load->model('vendor/lts_vendor');
    $this->load->model('vendor/lts_subscription');

    $vendor_info = $this->model_vendor_lts_vendor->getStoreInformation($this->customer->getId());

    if(!empty($vendor_info['address'])) {
      $data['vendor_address'] = $vendor_info['address'];
    } else {
      $data['vendor_address'] = '';
    }

   if(!empty($vendor_info['store_name'])) {
      $data['vendor_store_name'] = $vendor_info['store_name'];
    } else {
       $data['vendor_store_name'] = '';
    }

    $data['vendor_email'] = $this->customer->getEmail();
    $data['vendor_telephone'] = $this->customer->getTelephone();

    $plan_info = $this->model_vendor_lts_subscription->getVendonPlan($this->request->get['subscription_id']);

    if(!empty($plan_info['name'])) {
      $data['name'] = $plan_info['name'];
    } else {
       $data['name'] = '';
    }  

    if(!empty($plan_info['date_added'])) {
      $data['date_added'] = $plan_info['date_added'];
    } else {
       $data['date_added'] = '';
    }  

    if(!empty($plan_info['date_expire'])) {
      $data['date_expire'] = $plan_info['date_expire'];
    } else {
       $data['date_expire'] = '';
    }


    if(!empty($plan_info['join_fee'])) {
      $data['join_fee'] = $this->currency->format($plan_info['join_fee'], $this->config->get('config_currency'));;
    } else {
       $data['join_fee'] = '';
    } 

    if(!empty($plan_info['subscription_fee'])) {
      $data['subscription_fee'] = $this->currency->format( $plan_info['subscription_fee'], $this->config->get('config_currency'));;
    } else {
       $data['subscription_fee'] = '';
    }

    $data['total'] = $this->currency->format( $plan_info['subscription_fee'] + $plan_info['join_fee'], $this->config->get('config_currency'));;

    $data['title'] = $this->language->get('text_invoice');

    if ($this->request->server['HTTPS']) {
      $data['base'] = HTTPS_SERVER;
    } else {
      $data['base'] = HTTP_SERVER;
    }

    $this->load->controller('vendor/lts_header/script');
    $data['footer'] = $this->load->controller('common/footer');
    $data['header'] = $this->load->controller('common/header');
    $data['lts_column_left'] = $this->load->controller('vendor/lts_column_left');
    
    $this->response->setOutput($this->load->view('vendor/lts_subcription_invoice', $data));
  }

}
