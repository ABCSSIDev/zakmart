<?php

require_once(DIR_SYSTEM . '/library/vendor/PHPExcel.php');

class ControllerVendorLtsImport extends controller {

  public function index() {

    die;

    $phpExcel = new PHPExcel();

    if ($this->request->server['REQUEST_METHOD'] == 'POST') {

      if (is_uploaded_file($this->request->files['product']['tmp_name'])) {
        $content = file_get_contents($this->request->files['product']['tmp_name']);
      } else {
        $content = false;
      }


      $objPHPExcel = PHPExcel_IOFactory::load($this->request->files['product']['tmp_name']);

      $sheetDatas = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

      foreach ($sheetDatas as $value) {
        print_r($value['B']);
      }
    }

    $data['action'] = $this->url->link('vendor/lts_import');

    $this->load->controller('vendor/lts_header/script');
    $data['footer'] = $this->load->controller('common/footer');
    $data['header'] = $this->load->controller('common/header');
    $data['lts_column_left'] = $this->load->controller('vendor/lts_column_left');

    $this->response->setOutput($this->load->view('vendor/lts_import', $data));
  }
}