<?php
  Class ControllerVendorLtsMultiProduct extends Controller {

    private $error = array();

    public function index() {

      $this->load->language('vendor/lts_multi_product');

      $this->document->setTitle($this->language->get('heading_title'));

      $this->load->model('vendor/lts_product');

       if (!$this->config->get('module_lts_vendor_status')) {
        $this->response->redirect($this->url->link('error/not_found', '', true));
      }

      if (!$this->customer->getId() || !$this->customer->isVendor()) {
        $this->response->redirect($this->url->link('vendor/lts_login', '', true));
      }

       $this->getList();
    }


  protected function getList() {
    $this->load->controller('vendor/lts_header/script');


     if (!$this->config->get('module_lts_vendor_status')) {
      $this->response->redirect($this->url->link('error/not_found', '', true));
    }

    if (!$this->customer->getId() || !$this->customer->isVendor()) {
      $this->response->redirect($this->url->link('vendor/lts_login', '', true));
    }

    if (isset($this->request->get['filter_name'])) {
      $filter_name = $this->request->get['filter_name'];
    } else {
      $filter_name = '';
    }

    if (isset($this->request->get['filter_model'])) {
      $filter_model = $this->request->get['filter_model'];
    } else {
      $filter_model = '';
    }

    if (isset($this->request->get['filter_price'])) {
      $filter_price = $this->request->get['filter_price'];
    } else {
      $filter_price = '';
    }

    if (isset($this->request->get['filter_quantity'])) {
      $filter_quantity = $this->request->get['filter_quantity'];
    } else {
      $filter_quantity = '';
    }

    if (isset($this->request->get['filter_status'])) {
      $filter_status = $this->request->get['filter_status'];
    } else {
      $filter_status = '';
    }

    if (isset($this->request->get['sort'])) {
      $sort = $this->request->get['sort'];
    } else {
      $sort = 'pd.name';
    }

    if (isset($this->request->get['order'])) {
      $order = $this->request->get['order'];
    } else {
      $order = 'ASC';
    }

    if (isset($this->request->get['page'])) {
      $page = $this->request->get['page'];
    } else {
      $page = 1;
    }

    $url = '';

    if (isset($this->request->get['filter_name'])) {
      $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_model'])) {
      $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_price'])) {
      $url .= '&filter_price=' . $this->request->get['filter_price'];
    }

    if (isset($this->request->get['filter_quantity'])) {
      $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
    }

    if (isset($this->request->get['filter_status'])) {
      $url .= '&filter_status=' . $this->request->get['filter_status'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
        'text' => $this->language->get('text_home'),
        'href' => $this->url->link('vendor/lts_dashboard')
    );

    $data['breadcrumbs'][] = array(
        'text' => $this->language->get('heading_title'),
        'href' => $this->url->link('vendor/lts_product')
    );

    $data['add'] = $this->url->link('vendor/lts_product/add');
    $data['copy'] = $this->url->link('vendor/lts_product/copy');
    $data['delete'] = $this->url->link('vendor/lts_product/delete');

    $data['products'] = array();

    $filter_data = array(
        'filter_name' => $filter_name,
        'filter_model' => $filter_model,
        'filter_price' => $filter_price,
        'filter_quantity' => $filter_quantity,
        'filter_status' => $filter_status,
        'sort' => $sort,
        'order' => $order,
        'start' => ($page - 1) * $this->config->get('config_limit_admin'),
        'limit' => $this->config->get('config_limit_admin')
    );

    $this->load->model('vendor/lts_image');

    $product_total = $this->model_vendor_lts_product->getTotalExistProducts($filter_data);

    $results = $this->model_vendor_lts_product->getExistProducts($filter_data);

    // print_r($results);

    // die;

    foreach ($results as $result) {
      if (is_file(DIR_IMAGE . $result['image'])) {
        $image = $this->model_vendor_lts_image->resize($result['image'], 40, 40);
      } else {
        $image = $this->model_vendor_lts_image->resize('no_image.png', 40, 40);
      }

      $special = false;

      $product_specials = $this->model_vendor_lts_product->getProductSpecials($result['product_id']);
      $product_status = $this->model_vendor_lts_product->getVendorProductById($result['product_id']);
      foreach ($product_specials as $product_special) {
        if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
          $special = $this->currency->format($product_special['price'], $this->config->get('config_currency'));

          break;
        }
      }

      $data['products'][] = array(
          'product_id' => $result['product_id'],
          'image' => $image,
          'name' => $result['name'], 
          'model' => $result['model'],
          'price' => $this->currency->format($result['price'], $this->config->get('config_currency')),
          'special' => $special,
          'quantity' => $result['quantity'],
          'status' => $result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
          'edit' => $this->url->link('vendor/lts_multi_product/edit', '&product_id=' . $result['product_id'] . $url, true)
      );
    }

    if ($this->config->get('module_lts_vendor_status') && $this->config->get('module_lts_vendor_delete_product')) {

      $data['module_lts_vendor_delete_product'] = $this->config->get('module_lts_vendor_delete_product');
    }

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }

    if (isset($this->request->post['selected'])) {
      $data['selected'] = (array) $this->request->post['selected'];
    } else {
      $data['selected'] = array();
    }

    $url = '';

    if (isset($this->request->get['filter_name'])) {
      $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_model'])) {
      $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_price'])) {
      $url .= '&filter_price=' . $this->request->get['filter_price'];
    }

    if (isset($this->request->get['filter_quantity'])) {
      $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
    }

    if (isset($this->request->get['filter_status'])) {
      $url .= '&filter_status=' . $this->request->get['filter_status'];
    }

    if ($order == 'ASC') {
      $url .= '&order=DESC';
    } else {
      $url .= '&order=ASC';
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $data['sort_name'] = $this->url->link('vendor/lts_product');
    $data['sort_model'] = $this->url->link('vendor/lts_product');
    $data['sort_price'] = $this->url->link('vendor/lts_product');
    $data['sort_quantity'] = $this->url->link('vendor/lts_product');
    $data['sort_status'] = $this->url->link('vendor/lts_product');
    $data['sort_order'] = $this->url->link('vendor/lts_product');

    $url = '';

    if (isset($this->request->get['filter_name'])) {
      $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_model'])) {
      $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_price'])) {
      $url .= '&filter_price=' . $this->request->get['filter_price'];
    }

    if (isset($this->request->get['filter_quantity'])) {
      $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
    }

    if (isset($this->request->get['filter_status'])) {
      $url .= '&filter_status=' . $this->request->get['filter_status'];
    }

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    $pagination = new Pagination();
    $pagination->total = $product_total;
    $pagination->page = $page;
    $pagination->limit = $this->config->get('config_limit_admin');
    $pagination->url = $this->url->link('vendor/lts_product', '&page={page}');

    $data['pagination'] = $pagination->render();

    $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

    $data['filter_name'] = $filter_name;
    $data['filter_model'] = $filter_model;
    $data['filter_price'] = $filter_price;
    $data['filter_quantity'] = $filter_quantity;
    $data['filter_status'] = $filter_status;

    $data['sort'] = $sort;
    $data['order'] = $order;

    $data['header'] = $this->load->controller('common/header');
    $data['lts_column_left'] = $this->load->controller('vendor/lts_column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('vendor/lts_multi_product_list', $data));
  }

  public function add() {
      if (!$this->config->get('module_lts_vendor_status')) {
        $this->response->redirect($this->url->link('error/not_found', '', true));
      }

      if (!$this->customer->getId() || !$this->customer->isVendor()) {
        $this->response->redirect($this->url->link('vendor/lts_login', '', true));
      }
      
      $this->load->language('vendor/lts_product');

      $this->document->setTitle($this->language->get('heading_title'));

      $this->load->model('vendor/lts_product');

      if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

        $this->model_vendor_lts_product->addExistProduct($this->request->post);

        $this->response->redirect($this->url->link('vendor/lts_product'));
      }
      $this->getForm();
    }

    public function edit() {
    $this->load->controller('vendor/lts_header/script');

    $this->load->language('vendor/lts_product');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('vendor/lts_product');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

      
      $this->model_vendor_lts_product->editProduct($this->request->get['product_id'], $this->request->post);

      $this->session->data['success'] = $this->language->get('text_success');

      $url = '';

      if (isset($this->request->get['filter_name'])) {
        $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_model'])) {
        $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
      }

      if (isset($this->request->get['filter_price'])) {
        $url .= '&filter_price=' . $this->request->get['filter_price'];
      }

      if (isset($this->request->get['filter_quantity'])) {
        $url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
      }

      if (isset($this->request->get['filter_status'])) {
        $url .= '&filter_status=' . $this->request->get['filter_status'];
      }

      if (isset($this->request->get['sort'])) {
        $url .= '&sort=' . $this->request->get['sort'];
      }

      if (isset($this->request->get['order'])) {
        $url .= '&order=' . $this->request->get['order'];
      }

      if (isset($this->request->get['page'])) {
        $url .= '&page=' . $this->request->get['page'];
      }

      $this->response->redirect($this->url->link('vendor/lts_product'));
    }

    $this->getForm();
  }


  public function getForm() {

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
        'text' => $this->language->get('text_home'),
        'href' => $this->url->link('vendor/lts_dashboard')
    );

    $data['breadcrumbs'][] = array(
        'text' => $this->language->get('heading_title'),
        'href' => $this->url->link('vendor/lts_product')
    );

     $data['breadcrumbs'][] = array(
        'text' => $this->language->get('text_product_exist'),
        'href' => $this->url->link('vendor/lts_product/addExist')
    );


    if (isset($this->error['product'])) {
      $data['error_product'] = $this->error['product'];
    } else {
      $data['error_product'] = '';
    }

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->error['price'])) {
      $data['error_price'] = $this->error['price'];
    } else {
      $data['error_price'] = '';
    }

    if (isset($this->error['special'])) {
      $data['error_special'] = $this->error['special'];
    } else {
      $data['error_special'] = '';
    }

    
    if (!isset($this->request->get['product_id'])) {
      $data['action'] = $this->url->link('vendor/lts_product/add');
    } else {
      $data['action'] = $this->url->link('vendor/lts_product/edit', '&product_id=' . $this->request->get['product_id'], true);
    }

    if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      $product_info = $this->model_vendor_lts_product->getProduct($this->request->get['product_id']);
    }
    
    if (isset($this->request->post['product_id'])) {
      $data['product_id'] = $this->request->post['product_id'];
    } else {
      $data['product_id'] = 0;
    }

    if (isset($this->request->post['product'])) {
      $data['product'] = $this->request->post['product'];
    } elseif (!empty($product_info)) {
      $product_info = $this->model_vendor_lts_product->getProduct($product_info['product_id']);

      if ($product_info) {
        $data['product'] = $product_info['name'];
      } else {
        $data['product'] = '';
      }
    } else {
      $data['product'] = '';
    }

    if (isset($this->request->post['price'])) {
      $data['price'] = $this->request->post['price'];
    } elseif (!empty($product_info)) {
      $data['price'] = $product_info['price'];

    
    } else {
      $data['product'] = '';
    }
    if (isset($this->request->post['special'])) {
      $data['special'] = $this->request->post['special'];
    } else {
      $data['special'] = '';
    }

    $this->load->controller('vendor/lts_header/script');
    $data['footer'] = $this->load->controller('common/footer');
    $data['header'] = $this->load->controller('common/header');
    $data['lts_column_left'] = $this->load->controller('vendor/lts_column_left');


    $this->response->setOutput($this->load->view('vendor/lts_multi_product_form', $data));
  }

  public function search() {

   $this->load->controller('vendor/lts_header/script');

    if (!$this->config->get('module_lts_vendor_status')) {
      $this->response->redirect($this->url->link('error/not_found', '', true));
    }

    if (!$this->customer->getId() || !$this->customer->isVendor()) {
      $this->response->redirect($this->url->link('vendor/lts_login', '', true));
    }

    
    $json = array();

    if (isset($this->request->get['product'])) {
      $this->load->model('vendor/lts_product');
     
      if (isset($this->request->get['product'])) {
        $product = $this->request->get['product'];
      } else {
        $product = '';
      }

      $filter_data = array(
          'product' => $product,
          'start' => 0,
          'limit' => 5
      );

      $results = $this->model_vendor_lts_product->search($filter_data);

      foreach ($results as $result) {
        $json[] = array(
            'product_id' => $result['product_id'],
            'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
        );
      }
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  protected function validateForm() {
    if (!isset($this->request->post['product_id']) || empty($this->request->post['product_id'])) {
      $this->error['product'] = $this->language->get('error_product');
    }

    if ($this->model_vendor_lts_product->getTotalExistProducts($this->request->post['product_id'])) {
      $this->error['warning'] = $this->language->get('error_exists');
    }
    
  if ($this->model_vendor_lts_product->getTotalVendorProducts($this->request->post['product_id'])) {
      $this->error['warning'] = $this->language->get('error_exists');
    }

    if (empty($this->request->post['price'])) {
      $this->error['price'] = $this->language->get('error_price');
    }


    if ($this->error && !isset($this->error['warning'])) {
      $this->error['warning'] = $this->language->get('error_warning');
    }

    return !$this->error;
  }



















  }
