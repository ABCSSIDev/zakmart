<?php

class ControllerVendorLtsPincode extends Controller {

  public function check() {

    $json = array();

    if (!$this->config->get('module_lts_vendor_status')) {
      $this->response->redirect($this->url->link('error/not_found', '', true));
    }
        
    if (!$this->customer->getId() || !$this->customer->isVendor()) {
      $this->response->redirect($this->url->link('vendor/lts_login', '', true));
    }

    $this->load->model('vendor/lts_product');

    if(isset($this->request->get['product_id'])) {
      $product_id = $this->request->get['product_id'];
    } else {
      $product_id = '';
    }

    $pincodes = $this->model_vendor_lts_product->getProductPincode($product_id);

    if(!empty($pincodes)) {
      
        foreach ($pincodes as $pincode_id) {
          $pincode_info = $this->model_vendor_lts_product->getPincodeStatus($pincode_id, $this->request->post['pincode']);

          if(!empty($pincode_info)) {
            $json = array(
              'status' => 1,
              'message' => $this->language->get('text_available')
            );
          } else {
            $json = array(
              'status' => 0,
              'message' => $this->language->get('text_unavailable')
            );
          }
        }

    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }
}