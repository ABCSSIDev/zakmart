<?php

// Heading
$_['heading_title'] = 'Multi Products';

// Text
$_['text_success'] = 'Success: You have modified multi products!';
$_['text_list'] = 'Product List';
$_['text_add'] = 'Add Product';
$_['text_edit'] = 'Edit Product';
$_['text_filter'] = 'Filter';
$_['text_plus'] = '+';
$_['text_minus'] = '-';
$_['text_default'] = 'Default';
$_['text_option'] = 'Option';
$_['text_option_value'] = 'Option Value';
$_['text_percent'] = 'Percentage';
$_['text_amount'] = 'Fixed Amount';
$_['text_keyword'] = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';
$_['text_enabled'] = 'Enabled';
$_['text_disabled'] = 'Disabled';
$_['text_reset_form'] = 'Reset Form';

// Column
$_['column_name'] = 'Product Name';
$_['column_model'] = 'Model';
$_['column_image'] = 'Image';
$_['column_price'] = 'Price';
$_['column_quantity'] = 'Quantity';
$_['column_status'] = 'Status';
$_['column_action'] = 'Action';
	
// Entry
$_['entry_name'] = 'Product Name';
$_['entry_special'] = 'Special Price';

// Help
$_['help_product'] = '(Autocomplete)';

// Error
$_['error_warning'] = 'Warning: Please check the form carefully for errors!';
$_['error_exists']         = 'Warning: Product is already exists!';