<?php
$_['heading_title']			= 'Setting';

//tab
$_['tab_shipping']			= 'Shipping';
$_['tab_payment']			= 'Payment';
$_['tab_paypal']		    = 'Paypal';
$_['tab_bank_transfer']	    = 'Bank Transfer';



//text
$_['text_payment_details']	= 'Payment Detailts';
$_['text_success']           = 'Success: You have modified setting!';

//entry
$_['entry_paypal']		= 'Paypal email';
$_['entry_bankname']	= 'Bank Name';
$_['entry_accountno']   = 'Account no';
$_['entry_ifsc']		= 'IFSC Code';
$_['entry_account_holder'] = 'Account holder';


//entry
$_['entry_zip_to']			= 'Zip To'; 
$_['entry_zip_from']		= 'Zip From';
$_['entry_weight_to']		= 'Weight To';
$_['entry_weight_from']		= 'Weight From';
$_['entry_country']			= 'Country';
$_['entry_state']			= 'State';