<?php
$_['heading_title']    = 'LetsCms Vendor';

//Text
$_['text_extension']   = 'ส่วนขยาย';
$_['text_success']     = 'สำเร็จ: คุณได้ปรับเปลี่ยนโมดูลผู้ขาย!';
$_['text_edit']        = 'แก้ไขโมดูลหมวดหมู่';
$_['text_feedback']	   = 'เรียนผู้ใช้โปรดให้ข้อเสนอแนะที่มีค่ากับเราเกี่ยวกับผลิตภัณฑ์ของเรา ความคิดเห็นของคุณเป็นสิ่งสำคัญสำหรับเรา ';

//entry
$_['entry_vendor_registration']		= 'หน้าการลงทะเบียนผู้ขาย';
$_['entry_vendor_approval']			= 'อนุมัติผู้ขายอัตโนมัติ';
$_['entry_product_approval']		= 'อนุมัติผลิตภัณฑ์อัตโนมัติ';
$_['entry_category_approval']		= 'อนุมัติหมวดหมู่อัตโนมัติ';
$_['entry_review_action']			= 'ผู้ขายสามารถเพิ่ม / แก้ไข / ลบบทวิจารณ์';
$_['entry_vendor_name_in_cart']		= 'ชื่อผู้ขายในตะกร้า';

$_['entry_pincode_checker']			= 'ตัวตรวจสอบรหัสพิน / รหัสไปรษณีย์';
$_['entry_pincode_checker_required']= 'ต้องใช้ตัวตรวจสอบรหัสพิน / รหัสไปรษณีย์';
$_['entry_product_tab']				= 'อนุญาตแท็บผลิตภัณฑ์';
$_['entry_product_fields']			= 'อนุญาตฟิลด์ผลิตภัณฑ์';
$_['entry_product_status']			= 'อนุญาตสถานะผลิตภัณฑ์';
$_['entry_product_category']		= 'หมวดหมู่สินค้าที่จำเป็นสำหรับผู้ขาย';
$_['entry_admin_receive_mail_product_add'] = 'ผู้ดูแลระบบรับอีเมลหลังจากเพิ่มผลิตภัณฑ์';
$_['entry_delete_product_after_vendor_delete']	= 'ลบสินค้าหลังจากผู้ขายลบ';
$_['entry_seller_delete_product']	= 'ผู้ขายลบสินค้าจากร้านค้า';

$_['entry_vendor_can_change_status'] = 'ผู้ขายสามารถเปลี่ยนสถานะการสั่งซื้อ';
$_['entry_vendor_manage_order']		= 'อนุญาตให้ผู้ขายจัดการคำสั่งซื้อ';
$_['entry_vendor_receive_mail_product_purchase'] = 'Mail to Vendor จากการซื้อผลิตภัณฑ์';
$_['entry_order_status_change_by_vendor'] = 'แจ้งผู้ดูแลเมื่อสถานะการสั่งซื้อถูกเปลี่ยนโดยผู้ขาย';

$_['entry_customer_can_see_vendor_email'] = 'ลูกค้าสามารถเห็นผู้ขาย <b> อีเมล </b> ID';
$_['entry_customer_can_see_vendor_telephone'] = 'ลูกค้าสามารถเห็นผู้ขาย <b> โทรศัพท์ </b>';

$_['entry_status']	   = 'สถานะ';


//tab
$_['tab_general']		= 'ทั่วไป';
$_['tab_commission']	= 'คอมมิชชั่น';
$_['tab_product']		= 'ผลิตภัณฑ์';
$_['tab_order']			= 'คำสั่งซื้อ';
$_['tab_profile']		= 'การตั้งค่าโปรไฟล์';
$_['text_admin']		= 'ผู้ดูแลระบบ';
$_['text_vendor']		= 'ผู้ขาย';

//error
$_['error_permission'] = 'คำเตือน: คุณไม่ได้รับอนุญาตให้แก้ไขโมดูลผู้ขาย!';