<?php
$_['heading_title']			= 'การจัดการ Pincode';

//column
$_['column_name']			= 'ชื่อ';
$_['column_country']		= 'ประเทศ';
$_['column_state']			= 'รัฐ';
$_['column_status']			= 'สถานะ';
$_['column_action']			= 'การกระทำ';

//entry
$_['entry_country']			= 'ประเทศ';
$_['entry_state']			= 'โซน / รัฐ';
$_['entry_name']			= 'ชื่อ';
$_['entry_pincode']			= 'Pincode';
$_['entry_status']			= 'สถานะ';


//text
$_['text_autocomplete']	   	= 'เติมข้อความอัตโนมัติ';
$_['text_success']         	= 'ความสำเร็จ: คุณได้แก้ไขการจัดการรหัสผ่าน';
$_['text_add']				= 'เพิ่ม Pincode';
$_['text_edit']				= 'แก้ไข Pincode';
$_['text_list']				= 'รายการ Pincode';
$_['text_available']		= 'ว่าง';
$_['text_unavailable']		= 'ไม่พร้อมใช้งาน';


//error
$_['error_warning']          = 'คำเตือน: โปรดตรวจสอบข้อผิดพลาดอย่างระมัดระวัง!';
$_['error_category']		 = 'กรุณาเลือกหมวดหมู่';
$_['error_attributes']		 = 'กรุณาเลือกคุณสมบัติ';

