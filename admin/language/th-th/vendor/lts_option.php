<?php
$_['heading_title']		= 'การทำแผนที่ตัวเลือก';

//column
$_['column_category']	= 'หมวดหมู่';
$_['column_option']		= 'ตัวเลือก';
$_['column_action']		= 'การกระทำ';

//entry
$_['entry_category']	= 'หมวดหมู่';
$_['entry_option']		= 'ตัวเลือก';

//text
$_['text_autocomplete']	     = 'เติมข้อความอัตโนมัติ';
$_['text_add']               = 'เพิ่มการจับคู่ตัวเลือก';
$_['text_edit']              = 'แก้ไขการแมปตัวเลือก';
$_['text_list']				 = 'รายการการแมปตัวเลือก';

//error
$_['error_warning']          = 'คำเตือน: โปรดตรวจสอบข้อผิดพลาดอย่างระมัดระวัง!';
$_['error_permission']      = 'คำเตือน: คุณไม่ได้รับอนุญาตให้แก้ไขตัวเลือก!';
$_['error_options']			= 'โปรดเลือกตัวเลือก';
$_['error_category'] 		= 'กรุณาเลือกหมวดหมู่';


