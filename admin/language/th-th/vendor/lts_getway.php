<?php
$_['heading_title'] = 'SMS Getway';

//tab 
$_['tab_getway'] 				= 'การตั้งค่า SMS Getway';
$_['tab_order']			= 'สถานะการสั่งซื้อ';
$_['tab_login_registration']	= 'เข้าสู่ระบบ / การลงทะเบียน';
$_['tab_vendor']				= 'การแจ้งเตือนผู้ขาย';

//entry
$_['entry_getway_sender']		= 'ผู้ส่ง';
$_['entry_getway_status']		= 'สถานะ';
$_['entry_getway_url']			= 'Getway url';
$_['entry_apikey']				= 'Getway Api Key';

$_['entry_login_otp']			= 'เข้าสู่ระบบ OTP';
$_['entry_login_by']			= 'เข้าสู่ระบบโดย OTP';
$_['entry_forget_otp']			= 'ลืม OTP';
$_['entry_forget_by']			= 'ลืมโดย OTP';
$_['entry_register_otp']		= 'OTP การลงทะเบียน';
$_['entry_register_by']			= 'การลงทะเบียนโดย OTP';


$_['text_variable']				= 'ที่ {โทรศัพท์} ไม่มีหมายเลขโทรศัพท์';
$_['text_for_customer']			= 'แท็บนี้สำหรับลูกค้าเท่านั้น';
$_['text_yes']					= 'ใช่';
$_['text_no']					= 'ไม่';
$_['text_resend']				= 'ปุ่มส่งอีกครั้ง';
$_['text_success']    			= 'สำเร็จ: คุณได้แก้ไข SMS Getway';

$_['text_form']					= 'แก้ไข SMS Getway';

//error
$_['error_warning']          = 'คำเตือน: โปรดตรวจสอบข้อผิดพลาดอย่างระมัดระวัง!';