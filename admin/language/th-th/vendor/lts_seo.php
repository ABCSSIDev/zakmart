<?php
$_['heading_title']			= 'Seo';

//text
$_['text_add']				= 'เพิ่ม seo';
$_['text_edit']				= 'แก้ไข Seo';
$_['text_success']           = 'สำเร็จ: คุณได้แก้ไข seo!';

//entry
$_['entry_query']			= 'ข้อความค้นหา';
$_['entry_keyword']			= 'คำหลัก';

//column
$_['column_query']			= 'ข้อความค้นหา';
$_['column_keyword']		= 'คำหลัก';
$_['column_action']			= 'การกระทำ';

// Error
$_['error_warning']        = 'คำเตือน: โปรดตรวจสอบข้อผิดพลาดอย่างระมัดระวัง!';
$_['error_permission']     = 'คำเตือน: คุณไม่ได้รับอนุญาตให้ทำ seo!';
$_['error_query']          = 'ต้องมีการสืบค้น!';
$_['error_keyword']        = 'จำเป็นต้องใช้คำหลัก!';

