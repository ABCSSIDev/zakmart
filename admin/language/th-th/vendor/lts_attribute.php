<?php
$_['heading_title']		= 'การแมปแอตทริบิวต์';

//column
$_['column_category']	= 'หมวดหมู่';
$_['column_attribute']	= 'คุณสมบัติ';
$_['column_action']		= 'การกระทำ';

//entry
$_['entry_category']	= 'หมวดหมู่';
$_['entry_attribute']	= 'คุณสมบัติ';

//text
$_['text_autocomplete']	= 'เติมข้อความอัตโนมัติ';
$_['text_success']      = 'สำเร็จ: คุณได้แก้ไขแอททริบิวต์';
$_['text_add']			= 'เพิ่มการแมปแอตทริบิวต์';
$_['text_edit']			= 'แก้ไขการแมปแอตทริบิวต์';
$_['text_list']			= 'รายการการแมปแอตทริบิวต์';

//error
$_['error_warning']          = 'คำเตือน: โปรดตรวจสอบข้อผิดพลาดอย่างระมัดระวัง!';
$_['error_category']		 = 'กรุณาเลือกหมวดหมู่';
$_['error_attributes']		 = 'กรุณาเลือกคุณสมบัติ';

