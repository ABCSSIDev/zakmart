<?php
$_['heading_title']    		= 'คอมมิชชั่น';

//entry
$_['entry_category']   		= 'หมวดหมู่';
$_['entry_commission_type'] = 'ประเภทคอมมิชชั่น';
$_['entry_commission']	   	= 'คอมมิชชั่น';


//text
$_['text_fixed']	  = 'แก้ไข';
$_['text_percentage'] = 'เปอร์เซ็นต์';
$_['text_add']		  = 'เพิ่มค่าคอมมิชชัน';
$_['text_edit']		  = 'แก้ไขค่าคอมมิชชัน';
$_['text_list']		= 'รายชื่อคณะกรรมการ';
$_['text_select']	  = 'โปรดเลือก';
$_['text_percentage_text'] = 'เปอร์เซ็นต์';
$_['text_success']      = 'สำเร็จ: คุณได้แก้ไขค่าคอมมิชชัน';

//column

$_['column_categories']	  = 'หมวดหมู่';
$_['column_action']		  = 'การกระทำ';
$_['column_commission_type'] = 'ประเภทค่าคอมมิชชั่น';
$_['column_commission']	= 'คอมมิชชั่น';

//error 
$_['error_warning']          = 'คำเตือน: โปรดตรวจสอบข้อผิดพลาดอย่างระมัดระวัง!';
$_['error_category']         = 'รับประกัน: กรุณาเลือกหมวดหมู่';
$_['error_exists']			= 'รับประกัน: หมวดหมู่นี้มีอยู่แล้ว';
$_['error_commission']		= 'กรุณาใส่ค่าคอมมิชชัน!';
