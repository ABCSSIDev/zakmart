<?php
$_['heading_title'] 		= 'รายงานค่าคอมมิชชัน';

//text
$_['column_vendor_name'] 		= 'ชื่อผู้ขาย';
$_['column_product_name']		= 'ชื่อผลิตภัณฑ์';
$_['column_model']				= 'รุ่น';
$_['column_commission'] 		= 'คอมมิชชั่น';
$_['column_commission_type'] 	= 'ประเภทคอมมิชชั่น';
$_['column_total']				= 'รวม';
$_['column_quantity']			= 'ปริมาณ';
$_['column_price']				= 'ราคา';
$_['column_date_added']			= 'เพิ่มวันที่';

//error
$_['error_warning']          = 'คำเตือน: โปรดตรวจสอบข้อผิดพลาดอย่างระมัดระวัง!';