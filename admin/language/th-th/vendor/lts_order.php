<?php
$_['heading_title'] 		= 'รายงานคำสั่งซื้อ';
//text
$_['text_order_id'] 		= 'รหัสการสั่งซื้อ';
$_['text_vendor_name']		= 'ชื่อผู้ขาย';
$_['text_store_name']  		= 'Store';
$_['text_customer_name'] 	= 'ลูกค้า';
$_['text_product_name'] 	= 'ชื่อผลิตภัณฑ์';
$_['text_total']    		= 'รวม';
$_['text_date']				= 'วันที่';
$_['text_action'] 			= 'การกระทำ';
$_['text_status']			= 'สถานะ';

//view
$_['text_order_details']    = 'รายละเอียดการสั่งซื้อ';
$_['text_payment_method']	= 'วิธีการชำระเงิน:';
$_['text_date_added']		= 'เพิ่มวันที่';
$_['text_shipping_method']	= 'วิธีการจัดส่ง:';
$_['text_payment_address']	= 'ที่อยู่การชำระเงิน';
$_['text_shipping_address'] = 'ที่อยู่สำหรับจัดส่ง';

// Column
$_['column_order_id']            = 'รหัสการสั่งซื้อ';
$_['column_customer']            = 'ลูกค้า';
$_['column_status']              = 'สถานะ';
$_['column_date_added']          = 'เพิ่มวันที่';
$_['column_date_modified']       = 'แก้ไขวันที่';
$_['column_total']               = 'รวม';
$_['column_product']             = 'ผลิตภัณฑ์';
$_['column_model']               = 'รุ่น';
$_['column_quantity']            = 'ปริมาณ';
$_['column_price']               = 'ราคาต่อหน่วย';
$_['column_comment']             = 'ความคิดเห็น';
$_['column_notify']              = 'ลูกค้าแจ้งเตือน';
$_['column_location']            = 'ตำแหน่ง';
$_['column_reference']           = 'การอ้างอิง';
$_['column_action']              = 'การกระทำ';
$_['column_weight']              = 'น้ำหนักผลิตภัณฑ์';
$_['column_vendor']				 = 'ผู้ขาย';

//error
$_['error_warning']          = 'คำเตือน: โปรดตรวจสอบข้อผิดพลาดอย่างระมัดระวัง!';
