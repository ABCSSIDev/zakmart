<?php
$_['heading_title']			= 'หมวดหมู่ที่กำหนด';

//entry
$_['entry_category']		= "หมวดหมู่";
$_['entry_vendor']			= 'ผู้ขาย';

//text
$_['text_autocomplete']		= '(เติมข้อความอัตโนมัติ)';
$_['text_success']    		= 'สำเร็จ: คุณได้แก้ไขหมวดหมู่ที่ได้รับมอบหมาย!';

//column
$_['column_category']		= "หมวดหมู่";
$_['column_vendor']			= 'ผู้ขาย';
$_['column_action']			= 'การกระทำ';

//error
$_['error_warning']          = 'คำเตือน: โปรดตรวจสอบข้อผิดพลาดอย่างระมัดระวัง!';
$_['error_permission']       = 'คำเตือน: คุณไม่ได้รับอนุญาตให้แก้ไขผลิตภัณฑ์!';
$_['error_vendor']			= 'กรุณาเลือกผู้ขาย';
$_['error_vendor_category'] = 'กรุณาเลือกหมวดหมู่';