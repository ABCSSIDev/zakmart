<?php
$_['heading_title'] ='การตรวจสอบผู้ขาย';

//entry 
$_['entry_customer']	= 'ลูกค้า';
$_['entry_vendor']		= 'ผู้ขาย';
$_['entry_content']		= 'เนื้อหา';
$_['entry_status']		= 'สถานะ';

//text

$_['text_add']			= 'เพิ่มรีวิวผู้ขาย';
$_['text_edit']			= 'แก้ไขรีวิวผู้ขาย';

//error
$_['error_warning']     = 'คำเตือน: โปรดตรวจสอบข้อผิดพลาดอย่างระมัดระวัง!';
$_['error_customer']	= 'กรุณาเลือกลูกค้า!';
$_['error_vendor']		= 'กรุณาเลือกผู้ขาย';
$_['error_content']		= 'เนื้อหาต้องมากกว่า 1 และน้อยกว่า 255 ตัวอักษร!';

$_['text_vendor']		= 'ผู้ขาย';
$_['text_product']		= 'ผลิตภัณฑ์';
$_['text_customer']		= 'ลูกค้า';
$_['text_action']		= 'การกระทำ';
$_['text_text']			= 'ข้อความ';
$_['text_rating']		= 'อันดับ';
$_['text_date']			= 'เพิ่มวันที่';
