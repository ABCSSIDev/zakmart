<?php
// Heading
$_['heading_title']          = 'ผลิตภัณฑ์';

// Text
$_['text_success']           = 'สำเร็จ: คุณมีการแก้ไขผลิตภัณฑ์!';
$_['text_list']              = 'รายการสินค้า';
$_['text_add']               = 'เพิ่มผลิตภัณฑ์';
$_['text_edit']              = 'แก้ไขผลิตภัณฑ์';
$_['text_filter']            = 'ตัวกรอง';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'เริ่มต้น';
$_['text_option']            = 'ตัวเลือก';
$_['text_option_value']      = 'มูลค่าตัวเลือก';
$_['text_percent']           = 'เปอร์เซ็นต์';
$_['text_amount']            = 'จำนวนเงินคงที่';
$_['text_keyword']           = 'อย่าใช้ช่องว่างแทนที่ช่องว่างด้วย - และตรวจสอบให้แน่ใจว่า URL ของ SEO นั้นไม่ซ้ำกันทั่วโลก';

// Column
$_['column_vendor']			 = 'ชื่อผู้ขาย';
$_['column_name']            = 'ชื่อผลิตภัณฑ์';
$_['column_model']           = 'รุ่น';
$_['column_image']           = 'รูปภาพ';
$_['column_price']           = 'ราคา';
$_['column_quantity']        = 'ปริมาณ';
$_['column_status']          = 'สถานะ';
$_['column_action']          = 'การกระทำ';

// Entry
$_['entry_name']             = 'ชื่อผลิตภัณฑ์';
$_['entry_description']      = 'คำอธิบาย';
$_['entry_meta_title']       = 'ชื่อเมตาแท็ก';
$_['entry_meta_keyword']     = 'คำหลักของแท็ก Meta';
$_['entry_meta_description'] = 'คำอธิบายเมตาแท็ก';
$_['entry_store']            = 'ร้านค้า';
$_['entry_keyword']          = 'คำหลัก';
$_['entry_model']            = 'รุ่น';
$_['entry_sku']              = 'SKU';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'ตำแหน่ง';
$_['entry_shipping']         = 'ต้องมีการจัดส่งสินค้า';
$_['entry_manufacturer']     = 'ผู้ผลิต';
$_['entry_date_available']   = 'วันที่ว่าง';
$_['entry_quantity']         = 'ปริมาณ';
$_['entry_minimum']          = 'ปริมาณขั้นต่ำ';
$_['entry_stock_status']     = 'สถานะสินค้าหมด';
$_['entry_price']            = 'ราคา';
$_['entry_tax_class']        = 'ระดับภาษี';
$_['entry_points']           = 'คะแนน';
$_['entry_option_points']    = 'คะแนน';
$_['entry_subtract']         = 'ลบสต็อค';
$_['entry_weight_class']     = 'ระดับน้ำหนัก';
$_['entry_weight']           = 'น้ำหนัก';
$_['entry_dimension']        = 'ขนาด (ยาว x กว้าง x สูง)';
$_['entry_length_class']     = 'ความยาวระดับ';
$_['entry_length']           = 'ความยาว';
$_['entry_width']            = 'ความกว้าง';
$_['entry_height']           = 'ความสูง';
$_['entry_image']            = 'รูปภาพ';
$_['entry_additional_image'] = 'รูปภาพเพิ่มเติม';
$_['entry_customer_group']   = 'กลุ่มลูกค้า';
$_['entry_date_start']       = 'วันที่เริ่มต้น';
$_['entry_date_end']         = 'วันที่สิ้นสุด';
$_['entry_priority']         = 'ลำดับความสำคัญ';
$_['entry_attribute']        = 'คุณสมบัติ';
$_['entry_attribute_group']  = 'กลุ่มแอตทริบิวต์';
$_['entry_text']             = 'ข้อความ';
$_['entry_option']           = 'ตัวเลือก';
$_['entry_option_value']     = 'มูลค่าตัวเลือก';
$_['entry_required']         = 'จำเป็นต้องใช้';
$_['entry_status']           = 'สถานะ';
$_['entry_sort_order']       = 'เรียงลำดับ';
$_['entry_category']         = 'หมวดหมู่';
$_['entry_filter']           = 'ตัวกรอง';
$_['entry_download']         = 'ดาวน์โหลด';
$_['entry_related']          = 'ผลิตภัณฑ์ที่เกี่ยวข้อง';
$_['entry_tag']              = 'แท็กผลิตภัณฑ์';
$_['entry_reward']           = 'คะแนนสะสม';
$_['entry_layout']           = 'การแทนที่เค้าโครง';
$_['entry_recurring']        = 'โปรไฟล์ที่เกิดซ้ำ';

// Help
$_['help_sku']               = 'หน่วยรักษาสต็อค';
$_['help_upc']               = 'รหัสผลิตภัณฑ์สากล';
$_['help_ean']               = 'หมายเลขบทความยุโรป';
$_['help_jan']               = 'หมายเลขบทความภาษาญี่ปุ่น';
$_['help_isbn']              = 'หมายเลขหนังสือมาตรฐานสากล';
$_['help_mpn']               = 'หมายเลขชิ้นส่วนของผู้ผลิต';
$_['help_manufacturer']      = '(เติมข้อความอัตโนมัติ)';
$_['help_minimum']           = 'บังคับจำนวนขั้นต่ำที่สั่ง';
$_['help_stock_status']      = 'สถานะแสดงเมื่อสินค้าหมด';
$_['help_points']            = 'จำนวนแต้มที่ใช้ในการซื้อสินค้านี้ หากคุณไม่ต้องการให้สินค้านี้ซื้อโดยมีคะแนนเหลือ 0 ';
$_['help_category']          = '(เติมข้อความอัตโนมัติ)';
$_['help_filter']            = '(เติมข้อความอัตโนมัติ)';
$_['help_download']          = '(เติมข้อความอัตโนมัติ)';
$_['help_related']           = '(เติมข้อความอัตโนมัติ)';
$_['help_tag']               = 'คั่นด้วยเครื่องหมายจุลภาค';

// Error
$_['error_warning']          = 'คำเตือน: โปรดตรวจสอบข้อผิดพลาดอย่างระมัดระวัง!';
$_['error_permission']       = 'คำเตือน: คุณไม่ได้รับอนุญาตให้แก้ไขผลิตภัณฑ์!';
$_['error_name']             = 'ชื่อผลิตภัณฑ์จะต้องมากกว่า 1 และน้อยกว่า 255 ตัวอักษร!';
$_['error_meta_title']       = 'ชื่อ Meta ต้องมากกว่า 1 และน้อยกว่า 255 ตัวอักษร!';
$_['error_model']            = 'รุ่นผลิตภัณฑ์จะต้องมากกว่า 1 และน้อยกว่า 64 ตัวอักษร!';
$_['error_keyword']          = 'URL SEO ถูกใช้แล้ว!';
$_['error_unique']           = 'URL SEO ต้องไม่ซ้ำกัน!';

$_['text_vendor']			 = 'ผู้ขาย';
$_['text_approve']			 = 'อนุมัติ';
$_['text_disapproved']		 = 'ไม่อนุมัติ';
$_['text_pending']			 = 'รอดำเนินการ';
