<?php
$_['heading_title'] = 'ส่งออก / นำเข้า';

//tab
$_['tab_export']	= 'ส่งออก';
$_['tab_import']	= 'นำเข้า';
$_['tab_vendor']	= 'ผู้ขาย';
$_['tab_admin']		= 'การบริหาร';

//error
$_['error_warning']          = 'คำเตือน: โปรดตรวจสอบข้อผิดพลาดอย่างระมัดระวัง!';