<?php
$_['heading_title'] 		= 'Commission Report';

//text
$_['column_vendor_name'] 		= 'Vendor Name';
$_['column_product_name']		=  'Product Name';
$_['column_model']				= 'Model';
$_['column_commission'] 		= 'Commission';
$_['column_commission_type'] 	= 'Commission type';
$_['column_total']				= 'Total';
$_['column_quantity']			= 'Quantity';
$_['column_price']				= 'Price';
$_['column_status']				= 'Status';
$_['column_date_added']			= 'Date Added';
$_['column_pay']				= 'Pay';

$_['text_percentage']			= 'Percentage';
$_['text_fixed']				= 'Fixed';
$_['text_paid']					= 'Paid';
$_['text_unpaid']				= 'Unpaid';

//error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';