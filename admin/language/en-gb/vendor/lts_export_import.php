<?php
$_['heading_title'] = 'Export/Import';

// text
$_['text_xls']				= "XLS";
$_['text_xlsx'] 			= "XLSX";
$_['text_import_product']	= 'Import Product';
$_['text_update']			= 'Update';
$_['text_new']				= 'New';
$_['text_export_product']   = "Export Product";
$_['text_success']           = 'Success: You have export!';



//error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';