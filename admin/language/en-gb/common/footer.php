<?php
// Text
$_['text_footer']  = '<a href="https://abcsystems.com.ph">ABC Systems Solutions Inc.</a> &copy; 2009-' . date('Y') . ' All Rights Reserved. Powered by OpenCart';
$_['text_version'] = 'Version %s';