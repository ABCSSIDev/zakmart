<?php
class ControllerVendorLtsSeo extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('vendor/lts_seo');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('vendor/lts_seo');

		$this->getList();
	}

	public function add() {
		$this->load->language('vendor/lts_seo');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('vendor/lts_seo');

		if(($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$this->model_vendor_lts_seo->addSeo($this->request->post); 

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('vendor/lts_seo', 'user_token=' . $this->session->data['user_token'], true));
		}
 
		$this->getForm();
	}

	public function delete() {
		
		$this->load->language('vendor/lts_seo');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('vendor/lts_seo');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $seo_url_id) {
				$this->model_vendor_lts_seo->deleteSeo($seo_url_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('vendor/lts_seo', 'user_token=' . $this->session->data['user_token'], true));
		} 

		$this->getList();
	}

	public function edit() {

		$this->load->language('vendor/lts_seo');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('vendor/lts_seo');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			if(!isset($this->request->post['default_plan'])){
				$this->request->post['default_plan']=0;	
			}

			$this->model_vendor_lts_seo->editSeo($this->request->get['seo_url_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('vendor/lts_seo', 'user_token=' . $this->session->data['user_token'], true));
		}

		$this->getForm();
	}

	protected function getList() {

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'sd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('vendor/lts_seo', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['add'] = $this->url->link('vendor/lts_seo/add', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['delete'] = $this->url->link('vendor/lts_seo/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['seos'] = array();

		$filter_data = array(
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);

		$seo_total = $this->model_vendor_lts_seo->getTotalSeo($filter_data);

		$results = $this->model_vendor_lts_seo->getSeos($filter_data);

		foreach ($results as $result) {
			$data['seos'][] = array(
				'seo_url_id' => $result['seo_url_id'],
				'query'       => $result['query'],
				'keyword'     => $result['keyword'],
				'edit'       => $this->url->link('vendor/lts_seo/edit', 'user_token=' . $this->session->data['user_token'] . '&seo_url_id=' . $result['seo_url_id'] . $url, true)
			);	
		}

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = '';

		// if (isset($this->request->get['query'])) {
		// 	$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		// // }

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
			
		$data['sort_query'] = $this->url->link('vendor/lts_seo', 'user_token=' . $this->session->data['user_token'] . '&sort=query' . $url, true);
		$data['sort_keyword'] = $this->url->link('vendor/lts_seo', 'user_token=' . $this->session->data['user_token'] . '&sort=keyword' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $seo_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('vendor/lts_seo', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($seo_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($seo_total - $this->config->get('config_limit_admin'))) ? $seo_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $seo_total, ceil($seo_total / $this->config->get('config_limit_admin')));

		$data['header'] 	 = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] 	 = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('vendor/lts_seo_list', $data));
	}
	
	public function getForm() {
		$data['text_form'] = !isset($this->request->get['seo_url_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['query'])) {
			$data['error_query'] = $this->error['query'];
		} else {
			$data['error_query'] = array();
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}	

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('vendor/lts_seo', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		if (!isset($this->request->get['seo_url_id'])) {
			$data['action'] = $this->url->link('vendor/lts_seo/add', 'user_token=' . $this->session->data['user_token'], true);
		} else {
			$data['action'] = $this->url->link('vendor/lts_seo/edit', 'user_token=' . $this->session->data['user_token'] . '&seo_url_id=' . $this->request->get['seo_url_id'], true);
		}

		$data['cancel'] = $this->url->link('vendor/lts_seo', 'user_token=' . $this->session->data['user_token'], true);

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->request->get['seo_url_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$seo_info = $this->model_vendor_lts_seo->getSeo($this->request->get['seo_url_id']);
		}

		if (isset($this->request->post['query'])) {
			$data['query'] = $this->request->post['query'];
		} elseif (!empty($seo_info)) {
			$data['query'] = $seo_info['query'];
		} else {
			$data['query'] = '';
		}

		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($seo_info)) {
			$data['keyword'] = $seo_info['keyword'];
		} else {
			$data['keyword'] = '';
		}

		$data['header']		= $this->load->controller('common/header');
		$data['footer']		= $this->load->controller('common/footer');
		$data['column_left']= $this->load->controller('common/column_left');

		$this->response->setOutput($this->load->view('vendor/lts_seo_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'vendor/lts_seo')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->request->post['query']) {
			$this->error['query'] = $this->language->get('error_query');
		}	

		if (!$this->request->post['keyword']) {
			$this->error['keyword'] = $this->language->get('error_keyword');
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	public function validateDelete() {
		if (!$this->user->hasPermission('modify', 'vendor/lts_seo')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}