<?php
class ControllerVendorLtsCommissionReport extends controller {
	public function index() {
		$this->load->language('vendor/lts_commission_report');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('vendor/lts_commission_report');

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('vendor/lts_commission_report', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);


		$filter_data = array(
			// 'filter_name'	  => $filter_name,
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);

		$commission_report_total = $this->model_vendor_lts_commission_report->getTotalCommissionsReports($filter_data);

		$results = $this->model_vendor_lts_commission_report->getCommissionReport($filter_data);

		// print_r($results);

		// die;
		
		$this->load->model('vendor/lts_vendor');

		foreach($results as $result) {
			$vendor_info = $this->model_vendor_lts_vendor->getVendorName($result['vendor_id']);
			$data['commissions'][] = array(
				'vendor_name'	=> $vendor_info['name'],
				'name'			=> $result['name'],
				'model'			=> $result['model'],
				'quantity'		=> $result['quantity'],
				'price'			=> $this->currency->format($result['price'], $this->config->get('config_currency')),
				'commission_type' => $result['commission_type'] == 'p' ? $this->language->get('text_percentage') : $this->language->get('text_fixed'), 
				'commission'	=> $this->currency->format($result['commission'], $this->config->get('config_currency')),
				'status'		=> $result['status'] ? $this->language->get('text_paid') : $this->language->get('text_unpaid'), 
				'date_added'	=> $result['date_added'],
				'pay'       => $this->url->link('vendor/lts_commission_report/pay', 'user_token=' . $this->session->data['user_token'] . '&commision_report_id=' . $result['commision_report_id'] . $url, true) 
			);
		} 

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = '';

		// if (isset($this->request->get['name'])) {
		// 	$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		// }

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
			
		$data['sort_name'] = $this->url->link('vendor/lts_commission_report', 'user_token=' . $this->session->data['user_token'] . '&sort=v.vname' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer']		 = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('vendor/lts_commission_report', $data));

	}    

	public function pay() {
		$this->load->language('vendor/lts_commission_report');

		$this->document->setTitle($this->language->get('heading_title'));

		// $this->load->model('catalog/product');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_product->addProduct($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			$this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function getForm() {

		// $this->load->model('extension/purpletree_multivendor/subscriptionplan');
		$logger = new Log('error.log');
		$raw_post_data = file_get_contents('php://input');

		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
	foreach ($raw_post_array as $keyval) {
		$keyval = explode ('=', $keyval);
		if (count($keyval) == 2)
			$myPost[$keyval[0]] = urldecode($keyval[1]);
	}
		// read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
		$get_magic_quotes_exists = true;
	}
	
	foreach ($myPost as $key => $value) {
		if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
			$value = urlencode(stripslashes($value));
		} else {
			$value = urlencode($value);
		}
		$req .= "&$key=$value";
	}

		$ch = curl_init("https://www.paypal.com/cgi-bin/webscr");
	if ($ch == FALSE) {
		return FALSE;
	}

		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLINFO_HEADER_OUT, 1);

		// Set TCP timeout to 30 seconds

		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
		$res = curl_exec($ch);



	if (curl_errno($ch) != 0) // cURL error
		{
			$logger->write(date('[Y-m-d H:i e] ')."Can't connect to PayPal to validate IPN message: " . curl_error($ch));
		curl_close($ch);


		exit;
	} else {
			// Log the entire HTTP response if debug is switched on.
			$logger->write(date('[Y-m-d H:i e] ')."HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req ");
			$logger->write(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res");
			curl_close($ch);


	}

// Inspect IPN validation result and act accordingly
// Split response headers and payload, a better way for strcmp
	$payment_response = $res;

	$tokens = explode("\r\n\r\n", trim($res));
	$res = trim(end($tokens));
	if (strcmp ($res, "VERIFIED") == 0) {
	// assign posted variables to local variables
	foreach($_POST as $key=>$value) {
		$logger->write(date('[Y-m-d H:i e] ')."Paypal response for ".$key." is ".$value);
	}




echo "string";
	try {


		$payment_status = $_POST['payment_status'];



		if($payment_status == "Completed") {
			$status_id = 2;
		} else {
			$status_id = 1;
		}


print_r($status_id);


		die;

		$pending_reason = "";

		$subsject = '';

		if(isset($_POST['transaction_subject']) && $_POST['transaction_subject'] != '') {
			$subsject = ", Transaction Subject is ".$_POST['transaction_subject'];
		}

		if(isset($_POST['pending_reason']) && $_POST['pending_reason'] !='') {
		$pending_reason = ", Pending Reason is ".$_POST['pending_reason'];
		}

		$comment = "Payment Status is ".$_POST['payment_status'].", Verify Sign is ".$_POST['verify_sign']." ".$pending_reason.", IPN Track Id is ".$_POST['ipn_track_id'].$subsject;
		$txn_id = $_POST['txn_id'];
		$dataarraypaypal = array('invoice_id' => $_POST['custom'],
								 'status_id'  => $status_id,
								 'comment'  => $comment,
								'transaction_id'  => $txn_id
								);

		$this->model_extension_purpletree_multivendor_subscriptionplan->addSellerPaymentHistoryfrompaypal($dataarraypaypal);
	} catch(Exception $e){ 
		$logger->write("Something went wrong after payment from Paypal ".$e->getMessage()); 				

			}
// check whether the payment_status is Completed
	//$logger->write(date('[Y-m-d H:i e] '). "Verified IPN: $req ");
} else if (strcmp ($res, "INVALID") == 0) {
	// log for manual investigation
	// Add business logic here which deals with invalid IPN messages
	$logger->write(date('[Y-m-d H:i e] '). "Invalid IPN: $req");
}
























	// 	$data['header'] = $this->load->controller('common/header');
	// 	$data['column_left'] = $this->load->controller('common/column_left');
	// 	$data['footer']		 = $this->load->controller('common/footer');

	// 	$this->response->setOutput($this->load->view('vendor/lts_commission_pay', $data));

	}

}   