<?php

require_once( DIR_SYSTEM . '/library/vendor/PHPExcel.php' );

class ControllerVendorLtsExportImport extends controller {
	private $error = array();

	public function index() {
		$this->load->language("vendor/lts_export_import");

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('vendor/lts_export_import');

		$this->getForm(); 
	}

	public function export() {

		$this->load->language("vendor/lts_export_import");

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
				if($this->request->post['format']=='xls' || $this->request->post['format']=='xlsx') {

					$objPHPExcel = new PHPExcel();

					$this->exportExcelData($objPHPExcel,'General', 'product_description', 0);		
					
					$objPHPExcel->createSheet();
					$this->exportExcelData($objPHPExcel,'Data', 'product', 1);		
					
					$objPHPExcel->createSheet();
					$this->exportExcelData($objPHPExcel,'Attribute', 'product_attribute', 2);		
					
					$objPHPExcel->createSheet();
					$this->exportExcelData($objPHPExcel,'Option', 'product_option', 3);		
					
					$objPHPExcel->createSheet();
					$this->exportExcelData($objPHPExcel,'ProductOptionValue', 'product_option_value', 4);		
					
					$objPHPExcel->createSheet();
					$this->exportExcelData($objPHPExcel,'Discount', 'product_discount', 5);		
					 
					$objPHPExcel->createSheet();
					$this->exportExcelData($objPHPExcel,'Special', 'product_special', 6);		
					
					$objPHPExcel->createSheet();
					$this->exportExcelData($objPHPExcel,'Image', 'product_image', 7);		

					$objPHPExcel->createSheet();
					$this->exportExcelData($objPHPExcel,'Seo', 'seo_url', 8);		
					
					
					$filename = 'multi-vendor.xlsx';
	   
				    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				    header('Content-Disposition: attachment;filename="' . $filename . '"');
				    header('Cache-Control: max-age=0');
				    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				    $objWriter->setPreCalculateFormulas(false);
				    $objWriter->save('php://output');


				    $this->session->data['success'] = $this->language->get('text_success');

           			$this->response->redirect($this->url->link('vendor/lts_export_import', 'user_token=' . $this->session->data['user_token'], true));
			}
		}

		$this->getForm();
	}

	public function exportExcelData($objPHPExcel, $tab, $table_name, $index) {

		$this->load->model('vendor/lts_export_import');

		$objPHPExcel->setActiveSheetIndex($index);
		$column_name = array();
		$export_data=array();
		

		if($table_name == 'product_description') {
			$column_name = array();
			$export_data = array();	

			$results = $this->model_vendor_lts_export_import->getTab($table_name);

			foreach($results as $result) {
				$export_data[]=array('product_id'=> $result['product_id'],
					'language_id'=>$result['language_id'],
					'name'=>htmlspecialchars_decode($result['name']),
					'description'=>htmlspecialchars_decode($result['description']),
					'tag'=>htmlspecialchars_decode($result['tag']),
					'meta_title'=>htmlspecialchars_decode($result['meta_title']),
					'meta_description'=>htmlspecialchars_decode($result['meta_description']),
					'meta_keyword'=>htmlspecialchars_decode($result['meta_keyword']),
					);
			}
			$column_name=array('A'=>'product_id', 'B' => 'language_id', 'C'=>'name','D'=>'description','E'=>'tag','F'=>'meta_title','G'=>'meta_description','H'=>'meta_keyword');
		}

		if($table_name == 'product') {
			$column_name = array();
			$export_data = array();	

			$results = $this->model_vendor_lts_export_import->getTab($table_name);

			foreach($results as $result) {
				$export_data[]=array('product_id'=> $result['product_id'],
					'model' 		=> $result['model'],
					'sku' 		=> $result['sku'],
					'upc' 		=> $result['upc'],
					'ean' 		=> $result['ean'],
					'jan' 		=> $result['jan'],
					'isbn' 		=> $result['isbn'],
					'mpn' 		=> $result['mpn'],
					'location' 		=> $result['location'],
					'quantity' 		=> $result['quantity'],
					'stock_status_id' 		=> $result['stock_status_id'],
					'image' 		=> $result['image'],
					'manufacturer_id' 		=> $result['manufacturer_id'],
					'shipping' 		=> $result['shipping'],
					'price' 		=> $result['price'],
					'points' 		=> $result['points'],
					'tax_class_id' 		=> $result['tax_class_id'],
					'date_available' 		=> $result['date_available'],
					'weight' 		=> $result['weight'],
					'weight_class_id' 		=> $result['weight_class_id'],
					'length' 		=> $result['length'],
					'width' 		=> $result['width'],
					'height' 		=> $result['height'],
					'length_class_id' 		=> $result['length_class_id'],
					'subtract' 		=> $result['subtract'],
					'minimum' 		=> $result['minimum'],
					'sort_order' 		=> $result['sort_order'],
					'status' 		=> $result['status'],
					'viewed' 		=> $result['viewed'],
					'date_added' 		=> $result['date_added'],
					'date_modified' 		=> $result['date_modified'],
										
					);
			}

			$column_name=array('A'=>'product_id', 'B'=>'model', 'C'=>'sku','D'=>'upc', 'E'=>'ean', 'F'=>'jan', 'G'=>'isbn', 'H'=>'location', 'I'=>'quantity', 'J'=>	'stock_status_id', 'K'=>'image', 'L'=>'manufacturer_id', 'M'=>'shipping', 'N'=>'price', 'O'=>'points', 'P'=>'tax_class_id', 'Q'=>'date_available', 'R'=>'weight', 'S'=>'weight_class_id', 'T'=>'length', 'U'=>'width', 'V'=>'height', 'W'=>'length_class_id', 'X'=>'subtract', 'Y'=>'minimum', 'Z'=>'sort_order', 'AA'=>'status', 'AB'=>'viewed', 'AC'=>'date_added',  'AD'=>'date_modified');
		}

		if($table_name == 'product_attribute') {
			$column_name = array();
			$export_data = array();	

			$results = $this->model_vendor_lts_export_import->getTab($table_name);

			foreach($results as $result) {
				$export_data[]=array('product_id'=> $result['product_id'],
					'attribute_id'		=> $result['attribute_id'],
					'language_id'   	=> $result['language_id'],
					'text'          	=> htmlspecialchars_decode($result['text'])
					);
			}
			$column_name=array('A'=>'product_id','B'=>'attribute_id','C'=>'language_id','D'=>'text');
		}

		if($table_name == 'product_option') {
			$column_name = array();
			$export_data = array();	

			$results = $this->model_vendor_lts_export_import->getTab($table_name);


			foreach($results as $result) {
				$export_data[] = array('product_option_id' => $result['product_option_id'],
					'product_id'		=> $result['product_id'],
					'option_id'   		=> $result['option_id'],
					'value'          	=> $result['value'],
					'required'          => $result['required']
					);
			}
			$column_name=array('A'=>'product_option_id','B'=>'product_id','C'=>'option_id','D'=>'value', 'E' => 'required');
		}

		if($table_name == 'product_option_value') {
			$column_name = array();
			$export_data = array();	

			$results = $this->model_vendor_lts_export_import->getTab($table_name);

			foreach($results as $result) {
				$export_data[]=array('product_option_value_id' => $result['product_option_value_id'],
					'product_option_id'		=> $result['product_option_id'],
					'product_id'   		=> $result['product_id'],
					'option_id'   		=> $result['option_id'],
					'option_value_id'          	=> $result['option_value_id'],
					'quantity'          => $result['quantity'],
					'subtract'          => $result['subtract'],
					'price'          => $result['price'],
					'price_prefix'          => $result['price_prefix'],
					'points'          => $result['points'],
					'points_prefix'          => $result['points_prefix'],
					'weight'          => $result['weight'],
					'weight_prefix'          => $result['weight_prefix']
					);
			}
			$column_name=array('A'=>'product_option_value_id','B'=>'product_option_id','C'=>'product_id','D'=>'option_id', 'E'=>'option_value_id','F'=>'quantity','G'=>'subtract', 'H'=>'price','I'=>'price_prefix','J'=>'points', 'K'=>'points_prefix','L'=>'weight','M'=>'weight_prefix');
		}

		if($table_name == 'product_discount') {
			$column_name = array();
			$export_data = array();	

			$results = $this->model_vendor_lts_export_import->getTab($table_name);

			foreach($results as $result) {
				$export_data[]=array('product_discount_id'=> $result['product_discount_id'],
					'product_id' => $result['product_id'],
					'customer_group_id'=>$result['customer_group_id'],
					'quantity'=>$result['quantity'],
					'priority'=>$result['priority'],
					'price'=>$result['price'],
					'date_start'=>$result['date_start'],
					'date_end'=>$result['date_end'],
				);
			}
			$column_name=array('A'=>'product_discount_id', 'B' => 'product_id', 'C'=>'customer_group_id','D'=>'quantity','E'=>'priority','F'=>'price','G'=>'date_start','H'=>'date_end');
		}

		if($table_name == 'product_special') {
			$column_name = array();
			$export_data = array();	

			$results = $this->model_vendor_lts_export_import->getTab($table_name);

			foreach($results as $result) {
				$export_data[]=array('product_special_id'=> $result['product_special_id'],
					'product_id' => $result['product_id'],
					'customer_group_id'=>$result['customer_group_id'],
					'priority'=>$result['priority'],
					'price'=>$result['price'],
					'date_start'=>$result['date_start'],
					'date_end'=>$result['date_end'],
				);
			}
			$column_name=array('A'=>'product_special_id', 'B' => 'product_id', 'C'=>'customer_group_id','D'=>'priority','E'=>'price','F'=>'date_start','G'=>'date_end');
		}

		if($table_name == 'product_image') {
			$column_name = array();
			$export_data = array();	

			$results = $this->model_vendor_lts_export_import->getTab($table_name);

			foreach($results as $result) {
				$export_data[]=array('product_image_id'=> $result['product_image_id'],
					'product_id' => $result['product_id'],
					'image'=>$result['image'],
					'sort_order'=>$result['sort_order']
				);
			}
			$column_name=array('A'=>'product_image_id', 'B' => 'product_id', 'C'=>'image','D'=>'sort_order');
		}

		if($table_name == 'seo_url') {
			$column_name = array();
			$export_data = array();	

			$results = $this->model_vendor_lts_export_import->getSeoTab($table_name);

			foreach($results as $result) {
				$export_data[]=array('product_id'=> $result['product_id'],
					'query'	=>  $result['query'],
					'keyword' => $result['keyword']
					);
			}
			$column_name=array('A'=>'product_id', 'B' => 'query','C'=>'keyword');
		}

		if(!empty($column_name)) {	
			foreach($column_name as $key => $value) {

				$objPHPExcel->getActiveSheet()->setCellValue($key.'1',$value);	
				$objPHPExcel->getActiveSheet($index)->getColumnDimension($key)->setWidth(strlen($value)+4);
				$objPHPExcel->getActiveSheet($index)->getRowDimension(1)->setRowHeight(25);
				$objPHPExcel->getActiveSheet($index)->getStyle($key.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet($index)->getStyle($key.'1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				 $objPHPExcel->getActiveSheet($index)->getStyle($key.'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				  $objPHPExcel->getActiveSheet($index)->getStyle($key.'1')->getFill()->getStartColor()->setARGB('FFF');
				  $objPHPExcel->getActiveSheet($index)->getStyle($key.'1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN)->getColor()->setRGB('DDD'); 
				  $objPHPExcel->getActiveSheet($index)->getStyle($key.'1')->getFont()->setBold(true)->setName('Verdana')->setSize(10)->getColor()->setRGB('FFFFFF');
			}
		}

		$objPHPExcel->getActiveSheet()->freezePaneByColumnAndRow( 1, 2 );

		$i=0;

		if(!empty($export_data) && $export_data!=NULL) {
			foreach($export_data as $key => $value) {
				$ii = $i+2;	
				if($column_name != NULL) {
					foreach($column_name as $cell => $v){
						$objPHPExcel->getActiveSheet()->setCellValue($cell.''.$ii, $value[$v]);
					}
				}
				$i++;
			} 
		}
	 $objPHPExcel->getActiveSheet()->setTitle($tab);
	}

	public function import() {

		$this->load->model('vendor/lts_export_import');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->importValidateForm()) {

			if($this->request->files['file']['tmp_name'] != '') {	
				$excel_type = array('application/vnd.ms-excel', 'application/wps-office.xlsx', 'text/xls', 'text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

				if(in_array($this->request->files['file']["type"], $excel_type)) {
					$excel_filename = $this->request->files['file']['tmp_name'];
					$excel_type = PHPExcel_IOFactory::identify($excel_filename);
					$objReader = PHPExcel_IOFactory::createReader($excel_type);
					$objReader->setReadDataOnly(true);
					$objExcel = $objReader->load($excel_filename);
					
					$excel_sheet_name = $objExcel->getSheetNames($excel_filename);

					$excel_sheet_tab = array();

					if(!empty($excel_sheet_name)) {
						foreach($excel_sheet_name as $key => $tab){
							$objExcel->setActiveSheetIndexByName($tab);
							$excel_sheet_tab[$tab] = $objExcel->getActiveSheet()->toArray(null, true,true,true);
						}
					}

					if (array_key_exists("General",$excel_sheet_tab)) {

						$sheet_value = $this->excelSheetData($excel_sheet_tab['General']);

						$column_name = array('A' =>	'product_id', 'B' => 'language_id', 'C' => 'name', 'D' => 'description', 'E' => 'tag', 'F' =>  'meta_title', 'G' =>  'meta_description', 'H' => 'meta_keyword');
							//$gen = $this->CompareTable($column_name,$excel_sheet_tab['General'][1],'General');	


						try {
							if(!empty($sheet_value)) {
								foreach($sheet_value as $key => $value) {
									if($key != 1 ) {
										$data['product_description'][] = array(
											'product_id' 		=> $value['product_id'],
											'language_id' 		=> $value['language_id'],
											'name' 				=> $value['name'],
											'description' 		=> $value['description'],
											'tag' 				=> $value['tag'],
											'meta_title' 		=> $value['meta_title'],
											'meta_description' 	=> $value['meta_description'],
											'meta_keyword' 		=> $value['meta_keyword']
										);										
									}
								}

								$this->model_vendor_lts_export_import->updateGeneral($data['product_description']);
							}
							
						} catch (Exception $e) {
							
						}
					}

					if (array_key_exists("Data",$excel_sheet_tab)) {

						$sheet_value = $this->excelSheetData($excel_sheet_tab['Data']);

						$column_name=array('A'=>'product_id', 'B'=>'model', 'C'=>'sku','D'=>'upc', 'E'=>'ean', 'F'=>'jan', 'G'=>'isbn', 'H'=>'location', 'I'=>'quantity', 'J'=>	'stock_status_id', 'K'=>'image', 'L'=>'manufacturer_id', 'M'=>'shipping', 'N'=>'price', 'O'=>'points', 'P'=>'tax_class_id', 'Q'=>'date_available', 'R'=>'weight', 'S'=>'weight_class_id', 'T'=>'length', 'U'=>'width', 'V'=>'height', 'W'=>'length_class_id', 'X'=>'subtract', 'Y'=>'minimum', 'Z'=>'sort_order', 'AA'=>'status', 'AB'=>'viewed', 'AC'=>'date_added',  'AD'=>'date_modified');

						try {
							if(!empty($sheet_value)) {
								foreach($sheet_value as $key => $value) {
									if($key != 1 ) {
										$data['product_data'][] = array('product_id'=> $value['product_id'],
										'model' 		=> $value['model'],
										'sku' 		=> $value['sku'],
										'upc' 		=> $value['upc'],
										'ean' 		=> $value['ean'],
										'jan' 		=> $value['jan'],
										'isbn' 		=> $value['isbn'],
										// 'mpn' 		=> $value['mpn'],
										'location' 		=> $value['location'],
										'quantity' 		=> $value['quantity'],
										'stock_status_id' 		=> $value['stock_status_id'],
										'image' 		=> $value['image'],
										'manufacturer_id' 		=> $value['manufacturer_id'],
										'shipping' 		=> $value['shipping'],
										'price' 		=> $value['price'],
										'points' 		=> $value['points'],
										'tax_class_id' 		=> $value['tax_class_id'],
										'date_available' 		=> $value['date_available'],
										'weight' 		=> $value['weight'],
										'weight_class_id' 		=> $value['weight_class_id'],
										'length' 		=> $value['length'],
										'width' 		=> $value['width'],
										'height' 		=> $value['height'],
										'length_class_id' 		=> $value['length_class_id'],
										'subtract' 		=> $value['subtract'],
										'minimum' 		=> $value['minimum'],
										'sort_order' 		=> $value['sort_order'],
										'status' 		=> $value['status'],
										'viewed' 		=> $value['viewed'],
										'date_added' 		=> $value['date_added'],
										'date_modified' 		=> $value['date_modified']);
									}
								}

								$this->model_vendor_lts_export_import->updateProductData($data['product_data']);
							}
							
						} catch (Exception $e) {
							
						}
					}

					if (array_key_exists("Attribute",$excel_sheet_tab)) {

						$sheet_value = $this->excelSheetData($excel_sheet_tab['Attribute']);

						$column_name=array('A'=>'product_id','B'=>'attribute_id','C'=>'language_id','D'=>'text');
						
						try {
							if(!empty($sheet_value)) {
								foreach($sheet_value as $key => $value) {
									if($key != 1 ) {
										$data['product_attribute'][] = array('product_id'=> $value['product_id'],
											'attribute_id'		=> $value['attribute_id'],
											'language_id'   	=> $value['language_id'],
											'text'          	=> htmlspecialchars_decode($value['text'])
										);									
									}
								}

								$this->model_vendor_lts_export_import->updateAttribute($data['product_attribute']);
							}
							
						} catch (Exception $e) {
							
						}
					}

					if (array_key_exists("Option",$excel_sheet_tab)) {

						$sheet_value = $this->excelSheetData($excel_sheet_tab['Option']);

						$column_name=array('A'=>'product_option_id','B'=>'product_id','C'=>'option_id','D'=>'value', 'E' => 'required');
						
						try {
							if(!empty($sheet_value)) {
								foreach($sheet_value as $key => $value) {
									if($key != 1 ) {
										$data['product_option'][] = array('product_option_id' => $value['product_option_id'],
											'product_id'		=> $value['product_id'],
											'option_id'   		=> $value['option_id'],
											'value'          	=> $value['value'],
											'required'          => $value['required']
										);									
									}
								}

								$this->model_vendor_lts_export_import->updateOption($data['product_option']);
							}
							
						} catch (Exception $e) {
							
						}
					}

					if (array_key_exists("ProductOptionValue",$excel_sheet_tab)) {

						$sheet_value = $this->excelSheetData($excel_sheet_tab['ProductOptionValue']);

						$column_name=array('A'=>'product_option_value_id','B'=>'product_option_id','C'=>'product_id','D'=>'option_id', 'E'=>'option_value_id','F'=>'quantity','G'=>'subtract', 'H'=>'price','I'=>'price_prefix','J'=>'points', 'K'=>'points_prefix','L'=>'weight','M'=>'weight_prefix');
						
						try {
							if(!empty($sheet_value)) {
								foreach($sheet_value as $key => $value) {
									if($key != 1 ) {
										$data['product_option_value'][] = array('product_option_value_id' => $value['product_option_value_id'],
											'product_option_id'		=> $value['product_option_id'],
											'product_id'   		=> $value['product_id'],
											'option_id'   		=> $value['option_id'],
											'option_value_id'          	=> $value['option_value_id'],
											'quantity'          => $value['quantity'],
											'subtract'          => $value['subtract'],
											'price'          => $value['price'],
											'price_prefix'          => $value['price_prefix'],
											'points'          => $value['points'],
											'points_prefix'          => $value['points_prefix'],
											'weight'          => $value['weight'],
											'weight_prefix'          => $value['weight_prefix']
										);
									}
								}

								$this->model_vendor_lts_export_import->updateOptionValue($data['product_option_value']);
							}
							
						} catch (Exception $e) {
							
						}
					}



					
				}
			}			
		}

		// $this->getForm();



		$this->response->redirect($this->url->link('vendor/lts_export_import', 'user_token=' . $this->session->data['user_token'], true));	
	}

	protected function excelSheetData($tab=array()) {
		
		$field_data = array();
		$data = array();
		if(!empty($tab)){
			foreach($tab as $key=>$result){

				foreach($result as $field => $value){

				 	$field_data[$tab[1][$field]] = $value;
				}

				$data[$key]=$field_data;
			}

			return $data;
		}
	}

	public function CompareTable($tableFirst,$tableSecond,$logger,$tab){				
		$array_diff= array_diff($tableFirst,$tableSecond);

		if(empty($array_diff)) {
			return true;	
		} else {
			if(!empty($array_diff)){
				foreach($array_diff as $prod_key=> $prod_value){	
						$logger->write($tab." Tab - ".$prod_value." Column is Not match");	
				}
			}
			return false;
		}
	}

	public function getForm() {

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		 $data['action_export'] = $this->url->link('vendor/lts_export_import/export', 'user_token=' . $this->session->data['user_token'], true);

		 $data['action_import'] = $this->url->link('vendor/lts_export_import/import', 'user_token=' . $this->session->data['user_token'], true);


		$data['header']	= $this->load->controller('common/header');
		$data['column_left']	= $this->load->controller('common/column_left');
		$data['footer']	= $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('vendor/lts_export_import', $data));
	}

	protected function importValidateForm() {
		if (empty($this->request->post['file'])) {
			$this->error['warning'] = $this->language->get('error_file');
		}

		return !$this->error;

	}

	protected function validateForm() {
		if (!$this->user->hasPermission('access', 'vendor/lts_export_import')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return true;

	}
}
