<?php
Class ModelVendorLtsSeo extends Model {
	public function addSeo($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)0 . "', language_id = '" . (int)1 . "', query = '" . $this->db->escape($data['query']) . "', keyword = '" . $this->db->escape($data['keyword']) . "'");

	}

	public function editSeo($seo_url_id, $data) {

		$this->db->query("UPDATE " . DB_PREFIX . "seo_url SET store_id = '" . (int)0 . "', language_id = '" . (int)1 . "', query = '" . $this->db->escape($data['query']) . "', keyword = '" . $this->db->escape($data['keyword']) . "' WHERE seo_url_id = '" . (int)$seo_url_id . "'");

		$this->cache->delete('seo');

	}

	public function deleteSeo($seo_url_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "seo_url WHERE seo_url_id = '" . (int)$seo_url_id . "'");

		$this->cache->delete('seo');
	}

	public function getSeo($seo_url_id) {

		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "seo_url WHERE seo_url_id = '". (int)$seo_url_id ."' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getSeos($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "seo_url WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";

		$sort_data = array(
			'query',
			'keyword',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY query";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalSeo($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "seo_url";

		$sql .= " WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
}