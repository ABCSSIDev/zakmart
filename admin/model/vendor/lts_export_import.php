<?php

class ModelVendorLtsExportImport extends Model {
    public function getTab($table_name) {
        $query = $this->db->query("SELECT * FROM  ". DB_PREFIX ."$table_name ");
    
        return $query->rows;
    }

    public function getSeoTab($table_name) {

        $query=$this->db->query("SELECT product_id FROM " . DB_PREFIX . "product");

        $product_data = array_column($query->rows,"product_id");
        
        $query1 = $this->db->query("SELECT * FROM " . DB_PREFIX . "$table_name");

        $data = array();

        foreach($query1->rows as $key => $value){

            $product_id = explode('product_id=', $value['query']);

                if(isset($product_id[1]) && $product_id[1] != '') {
                        if(in_array($product_id[1], $product_data)){
                            $data[] = array('product_id'     =>  $product_id[1],
                                            'query'      =>  $value['query'],
                                            'keyword'       =>  $value['keyword']
                                        );  
                        }
                }
        }


        return $data;
    }

    public function updateProductData($data) {

        foreach ($data as $value) {
           $this->db->query("UPDATE " . DB_PREFIX . "product SET model = '" . $this->db->escape($value['model']) . "', sku = '" . $this->db->escape($value['sku']) . "', upc = '" . $this->db->escape($value['upc']) . "', ean = '" . $this->db->escape($value['ean']) . "', jan = '" . $this->db->escape($value['jan']) . "', isbn = '" . $this->db->escape($value['isbn']) . "', location = '" . $this->db->escape($value['location']) . "', quantity = '" . (int)$value['quantity'] . "', minimum = '" . (int)$value['minimum'] . "', subtract = '" . (int)$value['subtract'] . "', stock_status_id = '" . (int)$value['stock_status_id'] . "', date_available = '" . $this->db->escape($value['date_available']) . "', manufacturer_id = '" . (int)$value['manufacturer_id'] . "', shipping = '" . (int)$value['shipping'] . "', price = '" . (float)$value['price'] . "', points = '" . (int)$value['points'] . "', weight = '" . (float)$value['weight'] . "', weight_class_id = '" . (int)$value['weight_class_id'] . "', length = '" . (float)$value['length'] . "', width = '" . (float)$value['width'] . "', height = '" . (float)$value['height'] . "', length_class_id = '" . (int)$value['length_class_id'] . "', status = '" . (int)$value['status'] . "', tax_class_id = '" . (int)$value['tax_class_id'] . "', sort_order = '" . (int)$value['sort_order'] . "', date_modified = NOW() WHERE product_id = '" . (int)$value['product_id'] . "'");

            if (isset($value['image'])) {
                $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($value['image']) . "' WHERE product_id = '" . (int)$value['product_id'] . "'");
            }
        }
    }

    public function updateGeneral($product_description) {

    	foreach($product_description as $value) {
            $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$value['product_id'] . "'");

            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$value['product_id'] . "', language_id = '" . (int)$value['language_id'] . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
    		
    	}
    }

    public function updateAttribute($product_attribute) {
        foreach($product_attribute as $value) {
           $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$value['product_id'] . "'");

            $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$value['product_id'] . "' AND attribute_id = '" . (int)$value['attribute_id'] . "'");

           $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$value['product_id'] . "', attribute_id = '" . (int)$value['attribute_id'] . "', language_id = '" . (int)$value['language_id'] . "', text = '" .  $this->db->escape($value['text']) . "'");
            
        }
    }

    public function updateOption($product_option) {
        foreach($product_option as $value) {
           $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$value['product_id'] . "'");

           $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int)$value['product_option_id'] . "', product_id = '" . (int)$value['product_id'] . "', option_id = '" . (int)$value['option_id'] . "', required = '" . (int)$value['required'] . "'");
            
        }
    }    

    public function updateOptionValue($data) {
        foreach($data as $value) {
           $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$value['product_id'] . "'");

           $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_value_id = '" . (int)$value['product_option_value_id'] . "', product_option_id = '" . (int)$value['product_option_id'] . "', product_id = '" . (int)$value['product_id'] . "', option_id = '" . (int)$value['option_id'] . "', option_value_id = '" . (int)$value['option_value_id'] . "', quantity = '" . (int)$value['quantity'] . "', subtract = '" . (int)$value['subtract'] . "', price = '" . (float)$value['price'] . "', price_prefix = '" . $this->db->escape($value['price_prefix']) . "', points = '" . (int)$value['points'] . "', points_prefix = '" . $this->db->escape($value['points_prefix']) . "', weight = '" . (float)$value['weight'] . "', weight_prefix = '" . $this->db->escape($value['weight_prefix']) . "'");
            
        }
    }
}
