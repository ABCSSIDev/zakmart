<?php
// HTTP
define('HTTP_SERVER', 'http://zakmart.com/admin/');
define('HTTP_CATALOG', 'http://zakmart.com/');

// HTTPS
define('HTTPS_SERVER', 'http://zakmart.com/admin/');
define('HTTPS_CATALOG', 'http://zakmart.com/');

// DIR
define('DIR_APPLICATION', '/var/www/html/admin/');
define('DIR_SYSTEM', '/var/www/html/system/');
define('DIR_IMAGE', '/var/www/html/image/');
define('DIR_STORAGE', '/var/storage/');
define('DIR_CATALOG', '/var/www/html/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'zakuser');
define('DB_PASSWORD', 'P@ssw0rd1234');
define('DB_DATABASE', 'zakmart');
define('DB_PORT', '3306');
define('DB_PREFIX', 'zm_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
